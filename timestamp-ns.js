const getDate = (seconds, minutes, hours, days, months, years, multiplier) => {
    let result = new Date().getTime();
    if( seconds ) result += seconds;
    if( minutes ) result += minutes * 60;
    if( hours ) result += hours * 60 * 60;
    if( days ) result += days * 60 * 60 * 24;
    if( months ) result += months * 60 * 60 * 24 * 30;
    if( years ) result += years * 60 * 60 * 24 * 365;
    return (BigInt(result) * BigInt(multiplier)).toString();
};

module.exports.templateTags = [{
    name: 'TimestampNs',
    displayName: 'TimestampNs',
    description: 'Provides a current timestamp with addition in seconds format (when multiplier is set to 1)',
    args: [
        {
            displayName: 'Seconds to add',
            description: 'The seconds to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Minutes to add',
            description: 'The minutes to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Hours to add',
            description: 'The hours to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Days to add',
            description: 'The days to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Months to add',
            description: 'The months to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Years to add',
            description: 'The years to add to the date.',
            type: 'number',
            defaultValue: 0
        },
        {
            displayName: 'Multiplier',
            description: 'Multiplier of the output value.',
            type: 'number',
            defaultValue: "1000000"
        }
    ],
    run: async (context, seconds, minutes, hours, days, months, years, multiplier) => {
        return getDate(seconds, minutes, hours, days, months, years, multiplier);
    },
    liveDisplayName: (context) => {
        let values = context.map(c => c.value);
        return "timestamp (" + getDate(...values) + ")";
    }
}];
