# TimestampNs

Provides a current timestamp with addition in seconds format (when multiplier is set to 1)

Based on the [Date/Time Add](https://insomnia.rest/plugins/insomnia-plugin-date-time-add) plugin
